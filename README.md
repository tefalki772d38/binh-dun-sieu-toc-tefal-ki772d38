Thông tin chi tiết Bình đun siêu tốc Tefal KI772D38:

Tham khảo: https://hiyams.com/binh-dun-sieu-toc/binh-dun-sieu-toc-tefal/binh-dun-sieu-toc-tefal-ki772d38/

Bình đun siêu tốc Tefal KI772D38 thiết kế sang trọng, phù hợp với phong cách nhà bếp hiện đại và cổ điển.
Bình đun siêu tốc Tefal KI772D38 có thể chứa tối đa 1.7 lít, bạn có thể chuẩn bị nước để pha trà, cafe cho cả nhà.
Bình đun siêu tốc Tefal KI772D38 cực kỳ dễ sử dụng. Nhờ thiết kế đế xoay 360° giúp bình luôn được giữ cố định tại một chỗ. Đồng thời, thân bình dễ dàng khớp với đế xoay, dù bạn đặt bình ở vị trí nào.

Thông số kỹ thuật Bình đun siêu tốc Tefal KI772D38:
Công suất (W): 2400
Chức năng: Tự động ngắt điện khi nước sôi 100 độ
Dung tích tổng: 1.7L
Xuất xứ: Trung Quốc
Kích thước: 250 x 190 x 300 mm (DxRxC)
Khối lượng: 1.2 kg
Bảo hành: 2 năm